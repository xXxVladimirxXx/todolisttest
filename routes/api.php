<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'UserController@authenticate');
Route::post('register', 'UserController@register');

Route::middleware('jwt.auth')->group(function(){

    Route::post('tasks', 'TasksController@index');
    Route::post('task', 'TasksController@create');
    Route::post('task/done/{id}', 'TasksController@done');
    Route::put('tasks/{id}', 'TasksController@edit');
    Route::delete('tasks/{id}', 'TasksController@delete');

    Route::post('categories', 'CategoriesContller@index');
    Route::post('category', 'CategoriesContller@create');
    Route::put('categories/{id}', 'CategoriesContller@edit');
    Route::delete('categories/{id}', 'CategoriesContller@delete');
    
});