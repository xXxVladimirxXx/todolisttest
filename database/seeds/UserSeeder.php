<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
        /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // 'title', 'is_done', 'content'
        $items = [
            ['name' => 'admin', 'email' => 'admin@gmail.com', 'password' => bcrypt('password')],
            ['name' => 'sanya', 'email' => 'sanya@gmail.com', 'password' => bcrypt('password')],
            ['name' => 'vadim', 'email' => 'vadim@gmail.com', 'password' => bcrypt('password')]
        ];
        
        foreach ($items as $item) {
            User::create($item);
        }
    }
}
