<?php

use Illuminate\Database\Seeder;
use App\Task;

class TaskSeeder extends Seeder
{
        /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // 'title', 'is_done', 'content'
        $items = [
            ['title' => 'Have breakfast', 'is_done' => 0],           // 1 user
            ['title' => 'Do physical education', 'is_done' => 0],    // 1 user
            ['title' => 'Go to college', 'is_done' => 0],            // 2 user
            ['title' => 'Learn english', 'is_done' => 0],            // 2 user
            ['title' => 'Have lunch', 'is_done' => 0],               // 2 user
            ['title' => 'Go to the library', 'is_done' => 0],        // 1 user
            ['title' => 'Read a books', 'is_done' => 0],             // 1 user
            ['title' => 'Learn programing', 'is_done' => 0],         // 3 user
            ['title' => 'Check email', 'is_done' => 0],              // 3 user
            ['title' => 'Check social network', 'is_done' => 0],     // 3 user
            ['title' => 'Do the cleaning', 'is_done' => 0],          // 3 user 
            ['title' => 'Bring success to TodoList', 'is_done' => 0] // 1 user
        ];

        foreach ($items as $item) {
            Task::create($item);
        }
    }
}
