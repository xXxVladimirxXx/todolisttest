<?php

use Illuminate\Database\Seeder;

class PivotUserCategory extends Seeder
{
  public function run()
  {
    $items = [
        ['user_id' => 1, 'category_id' => 1],
        ['user_id' => 1, 'category_id' => 2],
        ['user_id' => 1, 'category_id' => 3],
        ['user_id' => 1, 'category_id' => 4],
        ['user_id' => 1, 'category_id' => 5],

        ['user_id' => 2, 'category_id' => 6],
        ['user_id' => 2, 'category_id' => 7],
        ['user_id' => 2, 'category_id' => 8],
        ['user_id' => 2, 'category_id' => 9],
        ['user_id' => 2, 'category_id' => 10],

        ['user_id' => 3, 'category_id' => 11],
        ['user_id' => 3, 'category_id' => 12],
        ['user_id' => 3, 'category_id' => 13],
        ['user_id' => 3, 'category_id' => 14],
        ['user_id' => 3, 'category_id' => 15]
    ];
    
    foreach ($items as $item) {
        DB::table('user_category')->insert($item);
    }
  }
}