<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
        /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            // 'title', 'content'
            // user 1
            ['title' => 'Study'],
            ['title' => 'Sport'],
            ['title' => 'Diet'],
            ['title' => 'internet'],
            ['title' => 'movement'],

            // user 2
            ['title' => 'Study'],
            ['title' => 'Sport'],
            ['title' => 'Diet'],
            ['title' => 'internet'],
            ['title' => 'movement'],

            // user 3
            ['title' => 'Study'],
            ['title' => 'Sport'],
            ['title' => 'Diet'],
            ['title' => 'internet'],
            ['title' => 'movement']
        ];

        foreach ($items as $item) {
            Category::create($item);
        }
    }
}
