<?php

use Illuminate\Database\Seeder;

class PivotTaskCategorySeeder extends Seeder
{
  public function run()
  {
    // 'title', 'is_done', 'content'
    $items = [
        ['task_id' => 1, 'category_id' => 3],
        ['task_id' => 2, 'category_id' => 5],

        ['task_id' => 3, 'category_id' => 10],
        ['task_id' => 4, 'category_id' => 6],
        ['task_id' => 5, 'category_id' => 8],
        ['task_id' => 6, 'category_id' => 5],
        ['task_id' => 7, 'category_id' => 1],

        ['task_id' => 8, 'category_id' => 11],
        ['task_id' => 9, 'category_id' => 14],
        ['task_id' => 10, 'category_id' => 14],
        ['task_id' => 11, 'category_id' => 12],
        ['task_id' => 12, 'category_id' => 4]
    ];
    
    foreach ($items as $item) {
        DB::table('task_category')->insert($item);
    }
  }
}