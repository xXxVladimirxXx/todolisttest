<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'is_done', 'content'
    ];

    /**
     * Категории, принадлежащие задаче.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'task_category');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_task');
    }
}
