<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\User;

class CategoriesContller extends Controller
{
    public function index(Request $request)
    {
        // Нужно получить tasks с текущим пользователем, приходится переберать user
        $user = User::whereId($request->id)->with('categories')->firstOrFail();
        $ids = $user->categories->pluck('id');

        $categories = Category::whereIn('id', $ids)->with('tasks')->get();

        return response()->json([
            'categories' => $categories
        ], 200);
    }

    public function create(Request $request)
    {
        $category = Category::create($request->category);;

        if (isset($request->user)) {
            $category->users()->detach();
            $category->users()->attach($request->user['id']);
        }

        if (isset($request->category['tasks']) && count($request->category['tasks']) > 0) {
            $category->tasks()->detach();
            $category->tasks()->attach($request->category['tasks']);
        }

        return response()->json([
          'category' => $category
        ], 200);
    }
    
    public function edit(Request $request, $id)
    {
        $category = Category::whereId($id)->first();
        $category->update($request->all());
        
        if (count($request->input('tasks')) > 0) {
            $category->tasks()->detach();
            $category->tasks()->attach($request->input('tasks'));
        }

        return response()->json([
          'category' => $category
        ], 200);
    }

    public function delete($id)
    {
        $category = Category::whereId($id)->with('tasks', 'users')->first();

        $category->tasks()->detach();
        $category->users()->detach();
        $category->delete();

        return response()->json(null, 204);
    }
}
