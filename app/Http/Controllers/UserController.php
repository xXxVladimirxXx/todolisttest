<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use JWTAuthException;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {

        $credentials = $request->only('email', 'password');

        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                  'response' => 'error',
                  'message' => 'invalid_email_or_password',
                ], 400);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
              'response' => 'error',
              'message' => 'failed_to_create_token',
            ], 500);
        }

        $user = JWTAuth::user();

        return response()->json(compact('token','user'));
    }

    public function register(Request $request)
    {

        $user = User::create($request->all());

        $password = bcrypt($request->password);
        $user->password = $password;

        $user->save();

        return response()->json([
          'user' => $user
        ], 200);
    }
}
