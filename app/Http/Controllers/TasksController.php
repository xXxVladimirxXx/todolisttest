<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        // Нужно получить tasks с текущим пользователем, приходится переберать user
        $user = User::whereId($request->id)->with('tasks')->firstOrFail();
        $ids = $user->tasks->pluck('id');

        $tasks = Task::whereIn('id', $ids)->with('categories')->get();

        return response()->json([
            'tasks' => $tasks
        ], 200);
    }

    public function create(Request $request)
    {
        $task = Task::create($request->task);;

        if (isset($request->user)) {
            $task->users()->attach($request->user['id']);
        }

        if (isset($request->task['categories']) && count($request->task['categories']) > 0) {
            $task->categories()->detach();
            $task->categories()->attach($request->task['categories']);
        }

        return response()->json([
          'task' => $task
        ], 200);
    }

    public function done($id)
    {
        $task = Task::whereId($id)->first();
        
        // условие ? значение1 : значение2
        $task->is_done == 0 ? $task->is_done = 1 : $task->is_done = 0;
        $task->save();

        return response()->json(200);
    }

    public function edit(Request $request, $id)
    {
        $task = Task::whereId($id)->first();
        $task->update($request->all());

        if (count($request->input('categories')) > 0) {
            $task->categories()->detach();
            $task->categories()->attach($request->input('categories'));
        }

        return response()->json([
          'task' => $task
        ], 200);
    }

    public function delete($id)
    {
        $task = Task::whereId($id)->with('categories', 'users')->first();

        $task->categories()->detach();
        $task->users()->detach();
        $task->delete();

        return response()->json(null, 204);
    }
}
