<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title', 'content'
    ];

    /**
     * Задачи, принадлежащие к категории.
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'task_category');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_category');
    }
}
