export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const token = store.state.users.token;
        const user = store.state.users.user;


        if(requiresAuth && !user) {
            next('/login');
        } else if(to.path == '/login' && user) {
            next('/');
        } else if(to.path == '/register' && user) {
            next('/');
        } else {
            next();
        }
    });

    axios.interceptors.response.use(null, (error) => {
        if (error.response.status == 401) {
            store.dispatch('users/logout');
            router.push('/login');
        }

        return Promise.reject(error);
    });
}