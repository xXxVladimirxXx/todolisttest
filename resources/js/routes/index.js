import Main  from '../Main';
import Tasks  from '../components/Tasks';
import Home  from '../components/Home';
import Admin  from '../components/admin/Admin';
import Task  from '../components/admin/Task';
import Category  from '../components/admin/Category';
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'

export const routes = [
    {
        path: '',
        component: Main,
        children: [
            {
                path: '/',
                name: 'home',
                component: Home,
            },
            {
                path: 'tasks',
                name: 'tasks',
                component: Tasks
            },
            {
                path: 'admin',
                name: 'admin',
                component: Admin,
                children: [
                    {
                        path: 'task',
                        name: 'adminTask',
                        component: Task,
                    }, 
                    {
                        path: 'category',
                        name: 'adminCategory',
                        component: Category,
                    }
                ],
            },
        ],
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    }
];
