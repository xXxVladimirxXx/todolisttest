import {router} from "../../app";
import axios from "axios/index";

export default {
  namespaced: true,
  state: {
    data: [],
    currentUser: []
  },
  actions: {
    initData({ commit }, user) {
      return new Promise((resolve) => {
        axios({
        url: '/api/tasks',
        data: user,
        method: 'POST'
        }).then(
          resp => {
              commit('setUser', user)

              let data  = (resp.data.tasks)
              commit('setData', data)
              
              resolve(true)
          }
        )
     })
    },
    add({ state, commit, dispatch }, task) {
     var data = {};
     data['task'] = task 
     data['user'] = state.currentUser

     return new Promise((resolve, reject) => {
      axios({ 
        url: '/api/task',
        data: data,
        method: 'POST',
      }).then(
        this.dispatch('tasks/initData', state.currentUser)
      )
     })
    },
    done({ state, commit, dispatch }, id) {
      axios({ 
        url: `/api/task/done/${id}`,
        method: 'POST',
      }).then(
        this.dispatch('tasks/initData', state.currentUser)
      )
    },
    update({ state, commit, dispatch }, data) {
      axios({ 
        url: `/api/tasks/${data.id}`,
        data: data,
        method: 'PUT',
      }).then(
        this.dispatch('tasks/initData', state.currentUser)
      )
    },
    delete({ state, commit }, dataId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/tasks/${dataId}`)
        .then((response) => {
          this.dispatch('tasks/initData', state.currentUser)
        })
      })
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    },
    setUser(state, user) {
      state.currentUser = user
    }
  },
  getters: {
    getData(state) {
        return state.data
    }
  }
}