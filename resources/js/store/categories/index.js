import {router} from "../../app";
import axios from "axios/index";

export default {
  namespaced: true,
  state: {
    data: [],
    currentUser: []
  },
  actions: {
    initData({ commit }, user) {
     return new Promise((resolve) => {
        axios({
        url: '/api/categories',
        data: user,
        method: 'POST'
        }).then(
          resp => {
              commit('setUser', user)

              let data  = (resp.data.categories)
              commit('setData', data)
              resolve(true)
          }
        )
     })
    },
    add({ state, commit, dispatch }, category) {    
     var data = {};
     data['category'] = category 
     data['user'] = state.currentUser
     
     return new Promise((resolve, reject) => {
      axios({ 
        url: '/api/category',
        data: data,
        method: 'POST',
      }).then(
        this.dispatch('categories/initData', state.currentUser)
      )
     })
    },
    update({ state, commit, dispatch }, data) {
      axios({ 
        url: `/api/categories/${data.id}`,
        data: data,
        method: 'PUT',
      }).then(
        this.dispatch('categories/initData', state.currentUser)
      )
    },
    delete({ state, commit }, dataId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/categories/${dataId}`)
        .then((response) => {
          this.dispatch('categories/initData', state.currentUser)
        })
      })
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    },
    setUser(state, user) {
      state.currentUser = user
    }
  },
  getters: {
    getData(state) {
        return state.data
    }
  }
}