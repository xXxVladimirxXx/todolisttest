import axios from 'axios'
import {router} from "../../app";

const bufToken = localStorage.getItem('user-token')
const bufUser = JSON.parse(localStorage.getItem('user'))

if (bufToken) {
  axios.defaults.headers.common.Authorization = `Bearer ${bufToken}`
}

export default {
  namespaced: true,
  state: {
    token: bufToken || "",
    user: bufUser || null,
    errors: [],
    roles: [],
    users: [],
    current: []
  },
  actions: {
    initData({ commit }) {
      return new Promise((resolve) => {
        axios({
          url: '/api/users',
          method: 'GET',
        }).then(
          resp => {
            let data = (resp.data.users) ? resp.data.users : []
            commit('setData', data)
            commit('setRoles', resp.data.roles)
            resolve(true)
          },
          err => {
            reject(err)
          }
        )
      })
    },
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({ url: "/api/login", data: user, method: "POST" })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user

            localStorage.setItem("user-token", token)
            localStorage.setItem("user", JSON.stringify(user))
            axios.defaults.headers.common.Authorization = `Bearer ${token}`
            commit("authSuccess", token)
            commit("setUser", user)

            router.push('/')

            resolve(resp)
          })
          .catch(err => {
            localStorage.removeItem("user-token")
            reject(err)
          });
      });
    },
    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({ url: "/api/register", data: user, method: "POST" })
          .then(resp => {

            router.push({ name: 'login' })
            resolve(resp)
          })
          .catch(err => {
            localStorage.removeItem("user-token")
            reject(err)
          });
      });
    },
    logout({ state }) {
      state.token = ""
      state.user = null;
      localStorage.removeItem("user-token")
      localStorage.removeItem("user");            
      router.push({ name: 'login' })
    }
  },
  mutations: {
    authSuccess(state, token) {
      state.token = token
    },
    setErrors(state, data) {
      state.errors = data
    },
    addUser(state, user) {
      state.users.push(user)
    },
    setUser(state, user) {
      state.user = user
    },
    setData(state, data) {
      state.users = data
    },
  },
  getters: {
    isAuthenticated(state) {
      return state.token !== ""
    },
    getUser(state) {
      return state.user
    },
    getUsers(state) {
      return state.users
    }
  }
};