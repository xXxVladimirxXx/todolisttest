import tasks from './tasks'
import users from './users'
import categories from './categories'

export default {
  modules: {
    tasks,
    categories,
    users
  }
}
